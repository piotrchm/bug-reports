**Tytuł:** Wprowadzenie znaków *#* lub *&*, powoduje jedynie wyszukanie frazy wprowadzonej przed tymi znakami

**Priorytet:** Medium

**Środowisko:**

OS: Windows 10 Professional

Opera 68


**Kroki:**

1. Otwórz stronę [https://www.filmweb.pl](https://www.filmweb.pl)
2. W polu wyszukiwania wprowadź frazę ze znakiem *#*,*&* lub oboma jednocześnie (np. *Fast & Furious*)

**Wynik:**

Fraza wprowadzona po *#* lub *&* nie zostaje wyszukana. Wyszukiwane są jedynie frazy wprowadzone przed znakami: *#*, *&* (zobacz załącznik *ucieta_fraza_po_#_lub_&*)

Wprowadzenie wszystkich fraz po znakach *#* lub *&*, powoduje otwarcie zakładki *BAZA FILMWEBU*

**Oczekiwany wynik**

 Zostaje wyświetlona lista wyników wyszukiwania dla wprowadzonej frazy
 
 **Załączniki**
 
 [ucieta_fraza_po_#_lub_&.mp4](https://gitlab.com/piotrchm/bug-reports/-/blob/master/Filmweb.pl/attachments/ucieta_fraza_po_%23_lub_&.mp4)


