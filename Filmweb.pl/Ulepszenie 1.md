**Tytuł:** Komunikat o wysłaniu lub niewysłaniu zgłoszenia nadużycia

**Opis**

Wysłanie zgłoszenia nadużycia przez użytkownika, nie daje żadnej odpowiedzi zwrotnej, dotyczącej wysłania/niewysłania zgłoszenia. Dlatego należy dodać komunikat o wysłaniu/niewysłaniu zgłoszenia przez użytkownika.
