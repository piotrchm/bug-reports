**Tytuł:** Automatyzacja usunięcia konta użytkownika

**Opis**

Procedura usunięcia konta użytkownika jest długa i narażona na pomyłkę administratora serwisu. Użytkownik, chcąc usunąć swoje konto, jest zobligowany do wysłania wiadomości e-mail ze swojego adresu e-mail, z którego założono konto. Konto jest usuwane manualnie przez pracowników serwisu. Wprowadzenie automatyzacji usuwania konta z poziomu jego ustawień, znacznie ułatwiłoby i przyśpieszyłoby tę procedurę. Stałaby się wtedy bardziej intuicyjna (obecnie trzeba samemu znaleźć informację, jak wygląda procedura usuwania konta oraz adres e-mail) oraz zminimalizowałaby możliwość wystąpienia nieprawidłowości (np. usunięto inne konto, wiadomość e-mail nie dotarła lub znajduje się w folderze SPAM).
