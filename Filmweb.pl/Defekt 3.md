**Tytuł:** Wyświetlenie komunikatu o błędzie, którego użytkownik nie może zamknąć po wysłaniu wiadomości z 74404 znakami (zarówno w tytule jak i treści)

**Priorytet:**
Low

**Środowisko**

OS: Windows 10 Professional

Opera 67

**Kroki:**

1.	Otwórz stronę [https://www.filmweb.pl](https://www.filmweb.pl)
2.	Zaloguj się do konta użytkownika (np. login: test147 | hasło: admin1)
3.	Utwórz ciąg 74404 znaków (np. za pomocą generatora http://www.unit-conversion.info/texttools/random-string-generator/ lub skorzystaj z pliku w załączniku- Tekst 74404 znaków.txt)
4.	Wyślij wiadomość do znajomego z 74404 znakami, zarówno w tytule jak i treści

**Wynik**

Wyświetla się okno z komunikatem: *Coś poszło nie tak. Spróbuj ponownie.* (zobacz 
załącznik *Komunikat_blad_wiadmość_74404 znaków.png*) Kliknięcie przez użytkownika X lub Ok nie wywołuje żadnej reakcji aplikacji.

**Oczekiwany wynik**

Aplikacja wyświetla komunikat o błędzie: *Coś poszło nie tak. Spróbuj ponownie.*, który użytkownik może zamknąć zarówno *X* oraz *Ok*.

**Załączniki**

[Tekst 74404 znaków.txt](https://gitlab.com/piotrchm/bug-reports/-/blob/master/Filmweb.pl/attachments/Tekst%2074404%20znak%C3%B3w.txt)

![Komunikat_blad_wiadmość_74404 znaków.png](https://gitlab.com/piotrchm/bug-reports/-/raw/master/Filmweb.pl/attachments/Komunikat_blad_wiadmo%C5%9B%C4%87_74404_znak%C3%B3w.png)