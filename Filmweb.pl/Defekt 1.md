**Tytuł:** Brak możlwiości odczytania wiadomości, która w temacie zawiera wyłącznie białe znaki

**Priorytet:** Medium

**Środowisko**

OS: Windows 10 Professional

Opera 67


**Kroki:**

1.	Otwórz stronę [https://www.filmweb.pl/](https://www.filmweb.pl/)
2.	Zaloguj się na konto użytkownika (np. login: test147 | hasło: admin1)
3.	Wyślij wiadomość do znajomego (np. Janusz Szymański [test148]), która w temacie zawiera tylko białe znaki (np. znaki spacji), w treści ciąg znaków (np. test)
4.	Zaloguj się do konta znajomego, do którego została wysłana wiadomość (np. dla użytkownika test147, znajomy: test148- login: test148 | hasło: admin1)
5.	Przejdź do zakładki *Poczta*
6.	Kliknij temat otrzymanej wiadomości

**Wynik**

Po kliknięciu tematu wiadomości aplikacja nie wyświetla żadnego rezultatu (zobacz załącznik *wiadmosc_temat_biale_znaki.png*), najechanie kursorem na temat wiadomości nie powoduje zmiany kursora (kursor jest domyślny).

**Oczekiwany wynik**

Najechanie kursorem na temat wiadomości powoduje jego zmianę na *pointer*. Kliknięcie tematu otrzymanej wiadomości powoduje wyświetlenie okna z wiadomością


**Załącznik**

![wiadmosc_temat_biale_znaki.png](Filmweb.pl/attachments/wiadmosc_temat_biale_znaki.png)
