**Tytuł:** Znaki od litery *ó* we frazie są usuwane po wyszukaniu frazy z literą *ó* i kliknięciu *Zobacz wszystkie*

**Priorytet:** Low

**Środowisko**

OS: Windows 10 Professional

Opera 68

**Kroki:**

1. Otwórz stronę [https://www.filmweb.pl](https://www.filmweb.pl)
2. Wyszukaj frazę zawierająca literę *ó* (np. podróż, podwójny, ósmy)
3. Przewiń stronę w dół do *Zobacz wszystkie*
4. Kliknij *Zobacz wszystkie*

**Wynik**

Wszystkie znaki wyszukiwanej frazy, znajdujące się po literze *ó* (włącznie) zostają usunięte z wprowadzonej frazy. (zobacz załącznik *usuniete_znaki_z_frazy_po_ó.mp4*)

Wyświetla się komunikat o błędzie: *Niestety, nie znaleźliśmy wyników. Zmień kryteria filtrowania*.

Jeżeli litera *ó* znajduje się na początku wyszukiwanej frazy, to użytkownik zostaje przekierowany do zakładki *BAZA FILMWEBU*

**Oczekiwany wynik:**

Kliknięcie *Zobacz wszystkie* na liście wyników wyszukiwania, powoduje wyświetlenie wszystkich elementów danego typu, zawierających wprowadzone słowo kluczowe


**Załączniki**

[usuniete_znaki_z_frazy_po_ó.mp4](https://gitlab.com/piotrchm/bug-reports/-/blob/master/Filmweb.pl/attachments/usuniete_znaki_z_frazy_po_%C3%B3.mp4)